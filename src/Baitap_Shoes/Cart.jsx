import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>
            <img src={item.image} width="80px" alt="" />
          </td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => this.props.handleQuantity(item, -1)}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-3">{item.quantity}</span>
            <button
              onClick={() => this.props.handleQuantity(item, 1)}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{item.quantity * item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.removeFromCart(item);
              }}
              className="btn btn-secondary"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Hình Ảnh</th>
              <th>Tên Sản Phẩm</th>
              <th>Giá</th>
              <th>Số Lượng</th>
              <th>Tổng Cộng</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
