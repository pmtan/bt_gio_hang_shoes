import React, { Component } from "react";

export default class Shoes_Item extends Component {
  render() {
    let { item, addToCart } = this.props;
    return (
      <div className="col-3 mt-3">
        <div className="card w-100 h-100">
          <h4 className="text-right m-0 p-0">
            <span className="badge badge-warning">${item.price}</span>
          </h4>
          <img className="card-img-top" src={item.image} alt="" />
          <div className="card-body d-flex flex-column justify-content-between">
            <h5 className="card-title">{item.name}</h5>
            <p className="card-text">
              {item.description.length < 100
                ? item.description
                : item.description.substring(0, 100) + "..."}
            </p>
            <button
              onClick={addToCart}
              href="#"
              className="btn btn-primary btn-sm"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
