import logo from './logo.svg';
import './App.css';
import Baitap_Shoes from './Baitap_Shoes/Baitap_Shoes';

function App() {
  return (
    <div className="App">
      <Baitap_Shoes/>
    </div>
  );
}

export default App;
