import React, { Component } from "react";
import Cart from "./Cart.jsx";
import { data_shoes } from "./Data_Shoes.js";
import Shoes_Item from "./Shoes_Item";

export default class Baitap_Shoes extends Component {
  state = {
    data: data_shoes,
    cart: [],
  };
  renderShoes = () => {
    return this.state.data.map((item, index) => {
      return (
        <Shoes_Item
          addToCart={() => this.addToCart(item)}
          key={index}
          item={item}
        />
      );
    });
  };

  addToCart = (shoes) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    let cloneCart = this.state.cart;
    if (index == -1) {
      let newItem = { ...shoes, quantity: 1 };
      cloneCart.push(newItem);
      this.setState({ cart: cloneCart });
    } else {
      cloneCart[index].quantity++;
      this.setState({ cart: cloneCart });
    }
  };

  removeFromCart = (shoes) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => shoes.id == item.id);
    cloneCart.splice(index, 1);
    this.setState({
      cart: cloneCart,
    });
  };

  handleQuantity = (shoes, value) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    let cloneCart = this.state.cart;
    if (index != -1) {
      cloneCart[index].quantity += value;
      this.setState({ cart: cloneCart });
    }
    cloneCart[index].quantity == 0 && cloneCart.splice(index, 1);
    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container py-5">
        {this.state.cart.length > 0 && (
          <Cart
            removeFromCart={this.removeFromCart}
            cart={this.state.cart}
            handleQuantity={this.handleQuantity}
          />
        )}
        <div className="row">{this.renderShoes()}</div>
      </div>
    );
  }
}
